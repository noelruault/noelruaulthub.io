# Python

## Virtual environments

```bash
mkdir ~/virtualenvironment

virtualenv ~/virtualenvironment/my_new_app

cd ~/virtualenvironment/my_new_app/bin

source activate

deactivate
```
