# Bash & Unix

### [How to kill a process running on particular port in Linux?](https://stackoverflow.com/questions/11583562/how-to-kill-a-process-running-on-particular-port-in-linux)

To list any process listening to the port 8080:

```
lsof -i:8080
```

To kill any process listening to the port 8080:

```
kill $(lsof -t -i:8080)
```

or more violently:

```
kill -9 $(lsof -t -i:8080)
```


## Daemon processes
```
Ctrl + Z
$ jobs
$ fg %1 # reach the process 
$ kill %1
```

## Synaptics tap enable
```
? sudo apt-get install xserver-xorg-core
sudo apt-get install xserver-xorg-input-synaptics
➜ synclient TapButton1=1                              
➜ synclient TapButton2=3                              
```


## Find & destroy MySQL service
```
netstat -tulpn | grep 3306
```
mysql start on ->
nano /etc/init/mysql.conf


## Git add modified only
https://stackoverflow.com/a/22437750