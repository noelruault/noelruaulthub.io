# Docker Docs

[Imprescindibles](https://docs.docker.com/get-started/part2/#the-app-itself):
- Dockerfile
- requirements.txt
- app.py

`$ sudo pip install -r requirements.txt`

Now run the build command. This creates a Docker image, which we’re going to tag using -t so it has a friendly name.

`$ docker build -t friendlyhello .`

Where is your built image? It’s in your machine’s local Docker image registry:

`$ docker images`

Run the app, mapping your machine’s port **4000** to the container’s **EXPOSE**d port **80** using `-p`:

`$ docker run -p 4000:80 friendlyhello`

> curl -v http://localhost:4000

Basically, you have three options:

- Neither specify `EXPOSE` nor `-p`.
- Only specify `EXPOSE`.
- Specify `EXPOSE` and `-p`.

If you do not specify any of those, the service in the container will not be accessible from anywhere except from inside the container itself.

If you `EXPOSE` a port, the service in the container is not accessible from outside Docker, but from inside other Docker containers. So this is good for inter-container communication.

If you `EXPOSE` and `-p` a port, the service in the container is accessible from anywhere, even outside Docker.

The reason why both are separated is IMHO because

- choosing a host port depends on the host and hence does not belong to the Dockerfile (otherwise it would be depending on the host),
- and often it's enough if a service in a container is accessible from other containers.

The [documentation](https://docs.docker.com/engine/reference/builder/#expose) explicitly states:

> The `EXPOSE` instruction exposes ports for use within links.

It also points you to how to [link containers](https://docs.docker.com/userguide/dockerlinks/), which basically is the inter-container communication I talked about.

PS: If you do `-p`, but do not `EXPOSE`, Docker does an implicit `EXPOSE`. This is because if a port is open to the public, it is automatically also open to other Docker containers. Hence `-p` includes `EXPOSE`. That's why I didn't list it above as a fourth case.

Now let’s run the app in the background, in detached mode:
`$ docker run -d -p 4000:80 friendlyhello`
You get the long container ID for your app. Your container is **running in the background.** 

 You can also see the abbreviated container ID with `docker ps`
`$ docker ps`

use docker stop to end the process, using the `CONTAINER ID`
`$ docker stop [docker-container-id]`


For an image which was pushed as:
`$ docker run -d -p 4000:80 friendlyhello`
Yo can tag it (in order to get a sub-image) with command:
`$ docker tag friendlyhello username/repository:tag`
In my case: `$ docker tag friendlyhello nruault/get-started:part1`


##Publish the image
`$ docker push username/repository:tag`
##Pull and run the image from the remote repository
`$ docker run -p 4000:80 username/repository:tag`
> If the image isn’t available locally on the machine, Docker will pull it from the repository.
> Note: If you don’t specify the :tag portion of these commands, the tag of :latest will be assumed.

#CheatSheet

```bash
-- docker-cheatsheet --
docker build -t friendlyname .   # Create image using this directory's Dockerfile
docker run -p 4000:80 friendlyname  # Run "friendlyname" mapping port 4000 to 80
docker run -d -p 4000:80 friendlyname    # Same thing, but in detached mode
docker ps                                 # See a list of all running containers
docker stop <hash>                     # Gracefully stop the specified container
docker ps -a                   # See a list of all containers, even the ones not running
docker kill <hash>                   # Force shutdown of the specified container
docker rm <hash>              # Remove the specified container from this machine
docker rm $(docker ps -a -q)           # Remove all containers from this machine
docker images -a                               # Show all images on this machine
docker rmi <imagename>            # Remove the specified image from this machine
docker rmi $(docker images -q)             # Remove all images from this machine
docker login                         # Log in this CLI session using your Docker credentials
docker tag <image> username/repository:tag  # Tag <image> for upload to registry
docker push username/repository:tag            # Upload tagged image to registry
docker run username/repository:tag                   # Run image from a registry
```


##Cleaning

### Delete all containers
`$ docker ps` or  `$docker ps -a`
`$ docker rm [CONTAINER ID] -f`

*cheat:*`docker rm $(docker ps -a -q) -f`

### Delete all images
`$ docker images`
`$ docker rmi [CONTAINER ID] -f`

*cheat:* `docker rmi $(docker images -q) -f`

##SSH to docker
`$ docker container exec -it control /bin/bash`

##Run Alejandro's script 
`$ ./dbss/docker/run.sh create`

```
$ docker container exec -it queue /bin/bash
$ nano /mnt/scripts/setup_rmq.sh
...
$ chmod 755 /mnt/scripts/setup_rmq.sh
$ exit
$ docker container exec -it queue /mnt/scripts/setup_rmq.sh
```



