# Notebook

## Interview
- https://github.com/jwasham/coding-interview-university
- https://github.com/yangshun/tech-interview-handbook
- https://github.com/andreis/interview

## Courses
- https://learn-anything.xyz/
- https://hackr.io/
- https://www.udemy.com/
- https://zealdocs.org/
- https://github.com/explore
- https://learnxinyminutes.com/

## Methodology
- https://12factor.net/es/

## Languages
- https://github.com/kamranahmedse/developer-roadmap

## Bash
- https://ss64.com/bash/syntax-keyboard.html
- Regex: https://regex101.com/
- Configuration: https://medium.com/@caulfieldOwen/youre-missing-out-on-a-better-mac-terminal-experience-d73647abf6d7

### Emacs
- https://github.com/syl20bnr/spacemacs
- http://caiorss.github.io/Emacs-Elisp-Programming/

## Git
- https://chris.beams.io/posts/git-commit/
- https://www.atlassian.com/git/tutorials
- https://maxbittker.github.io/code-review/
- https://stash.qvantel.net/users/migarcia/repos/dbss-reviewing/browse
- https://medium.com/unpacking-trunk-club/designing-awesome-code-reviews-5a0d9cd867e3

## UML
- https://www.draw.io/

## Front-end
### CSS Grids
- https://www.reddit.com/r/web_design/comments/6p2ytb/css_grid_changes_everything/
- https://css-tricks.com/snippets/css/complete-guide-grid/
- http://cssgridgarden.com/
- https://www.smashingmagazine.com/2017/10/css-grid-challenge-2017-winners/
- http://jonibologna.com/spring-into-css-grid/
- https://gridbyexample.com/examples/

### CSS transitions
- https://robots.thoughtbot.com/transitions-and-transforms
- http://ianlunn.github.io/Hover/

### 3rd party addons
- https://codyhouse.co/
- http://navnav.co/
- https://tympanus.net/codrops/

### Papers
- https://www.shopify.com/partners/blog/using-animation-to-improve-mobile-app-user-experience?utm_source=Shopify+Partner+Blog&utm_campaign=24c0d205f1-shopify-partners-daily-blog&utm_medium=email&utm_term=0_54a79a9e4c-24c0d205f1-352453289&mc_cid=24c0d205f1&mc_eid=1bdee5c40f

### Front ideas
- https://tympanus.net/codrops/2011/02/27/25-examples-of-interesting-and-beautiful-navigation/
- https://www.shopify.com/partners/blog/web-design-portfolio-inspiration
- https://tympanus.net/Development/HoverEffectIdeas/
- https://www.awwwards.com/30-great-websites-with-parallax-scrolling.html
- https://tympanus.net/Tutorials/CaptionHoverEffects/index2.html
- https://threejs.org/examples/webgl_panorama_equirectangular.html
- https://templated.co/
- https://codyhouse.co/gem/css-faq-template/
- https://tympanus.net/Tutorials/CSS3FullscreenSlideshow/index.html
- http://freefrontend.com/css-menu/

## Graphical designs
- https://dribbble.com/

## Back-end
### Python
- Python google docs: https://developers.google.com/edu/python/
- Python tricks: https://www.youtube.com/watch?v=VBokjWj_cEA
- https://pep8.org/#programming-recommendations
- LPTHW: https://github.com/chris-void/pyway/blob/master/Learn%20Python%20The%20Hard%20Way%2C%203rd%20Edition%20.pdf
- virtualenv: https://virtualenv.pypa.io/en/stable/userguide/
- Python patterns: http://python-patterns.guide/
- Sublime + Python: https://realpython.com/blog/python/setting-up-sublime-text-3-for-full-stack-python-development/
- Solving with Algorithms and Data Structures: https://interactivepython.org/runestone/static/pythonds/index.html
- Design Patterns: https://github.com/kamranahmedse/design-patterns-for-humans

#### Django
- http://ccbv.co.uk/

## Liquid (Shopify)
- http://cheat.markdunkley.com/


## Code utilities
- Terminal sharing: https://tmate.io/
- Terminal smart search: https://github.com/junegunn/fzf
- Simplified man pages: https://github.com/tldr-pages/tldr

### ZSH
- https://github.com/denysdovhan/spaceship-prompt

## OS utilities
- Floating youtube: https://chrome.google.com/webstore/detail/floating-for-youtube/jjphmlaoffndcnecccgemfdaaoighkel

## Ebooks and notes
- https://www.epublibre.org/inicio/index
- http://books.goalkicker.com/
- https://codeowl.io/#
- https://github.com/careercup/CtCI-6th-Edition
- https://0xword.com

## Trolling Python
- https://speakerdeck.com/anler/learning-by-trolling
